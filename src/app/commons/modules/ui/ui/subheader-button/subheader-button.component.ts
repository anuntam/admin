import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-subheader-button',
  templateUrl: './subheader-button.component.html',
  styleUrls: ['./subheader-button.component.scss']
})
export class SubheaderButtonComponent {

  @Input() icon: string | null = null;

  @Input() title: string | undefined;
}
