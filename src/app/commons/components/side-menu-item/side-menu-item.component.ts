import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

export interface SubMenuItem {
  path: string;
  text: string;
}

export interface MenuItem {
  path: string;
  text: string;
  icon: string;
  children?: SubMenuItem[];
}

@Component({
  selector: 'app-side-menu-item',
  templateUrl: './side-menu-item.component.html',
  styleUrls: ['./side-menu-item.component.scss']
})
export class SideMenuItemComponent {

  @Input() menu?:MenuItem;

  expended: boolean = false;

  activeChild?: SubMenuItem;

  constructor(private router: Router) {}

  onMenuItemClick() {
    if (this.menu?.children) {
      this.expended = !this.expended;
      this.activeChild = this.menu?.children[0];
      this.router.navigateByUrl(this.menu?.children[0].path)
    } else if (this.menu) {
      this.router.navigateByUrl(this.menu?.path)
    }
  }

  isChildActive(child: SubMenuItem): boolean {
    return child === this.activeChild;
  }
}
