import { EventEmitter, Injectable } from '@angular/core';
import { AuthService } from './auth.service';

interface Project {
  id: string;
  name: string;
  accounts: number;
}

@Injectable({
  providedIn: 'root'
})
export class WorkspaceService {

  projects: Project[] = [];
  
  private currentProject: Project | undefined;

  onProjectChange: EventEmitter<Project> = new EventEmitter(true);

  set project(project:Project) {
    localStorage.setItem('current_project', project.id);
    this.currentProject = project;
    this.onProjectChange.emit(project);
  }

  get project(): Project | undefined {
    return this.currentProject;
  }

  path: string[] = ["Media", "Recycle Bin"];

  constructor(private auth: AuthService){
    this.fetchProjects()
  }
  
  async fetchProjects() {
    return fetch(
      "http://localhost:8000/app/projects",
      {
        headers: {
          'Authorization': 'Bearer ' + this.auth.accessToken
        }
      }
    ).then((response) => response.json())
     .then(projects => {
      
      this.projects = projects;
      
      if (projects && projects.length) {
        this.currentProject = this.projects.find(item => item.id === localStorage.getItem("current_project"));
      }

      if (!this.currentProject) {
        this.currentProject = this.projects[0];
      }

      this.onProjectChange.emit(this.currentProject);

     })
  }
}
