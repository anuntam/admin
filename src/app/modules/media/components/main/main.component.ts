import { Component } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/commons/services/auth.service';
import { WorkspaceService } from 'src/app/commons/services/workspace.service';
import { MediaFile } from '../media-renderer/media-renderer.component';

@Component({
  selector: 'app-media-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainMediaComponent {

  limit = 20;
  offset = 0;

  count = 0;
  items: any[] = [];

  active: MediaFile | null = null;

  mimeFilter: string = ""

  keywordFilter: string = "";

  projectChange: Subscription | null = null;

  constructor(private auth: AuthService, private workspace: WorkspaceService){
    workspace.path = ["Media"]
  }

  ngOnInit() {
    this.fetchMediaList();
    this.projectChange = this.workspace.onProjectChange.subscribe((project)=>{
      this.active = null;
      this.fetchMediaList()
    })
  }

  onChangeKeyword(keyword: string) {
    this.active = null;
    this.keywordFilter = keyword;
    this.fetchMediaList();
  }

  onChangeMimeFilter(mime: string) {
    this.mimeFilter = mime;
    this.fetchMediaList();
  }

  async fetchMediaList() {
    
    if (!this.workspace.project) {
      return;
    }

    let url;


    url = "http://localhost:8000/media?count=1&context="+this.workspace.project?.id

    if (this.keywordFilter != "" ) {
      url += "&search="+this.keywordFilter;
    }
    
    this.count = await this.auth.fetch(url).then(response => response.json()); 

    url = "http://localhost:8000/media?context="+this.workspace.project?.id+"&offset="+this.offset+"&limit="+this.limit

    if (this.mimeFilter != "" && this.mimeFilter != "*" ) {
      url += "&mime="+this.mimeFilter;
    }

    if (this.keywordFilter != "" ) {
      url += "&search="+this.keywordFilter;
    }

    this.items = await this.auth.fetch(url).then(response => response.json());
  }

  onFileUploadChange(event: any) {
    
    const files: File[] = event.target.files;
    
    if (!this.workspace.project) {
      alert('Please select a project')
      return
    }

    if (files.length == 0) {
      alert('No files to upload')
      return
    }
    
    const data = new FormData();

    data.append("media", files[0]);
    data.set("context", this.workspace.project.id)

    fetch("http://localhost:8000/media",{
      method:'PUT',
      headers: {
        'Authorization': 'Bearer ' + this.auth.accessToken,
      },
      body: data,
    }).then((response)=>{
      if(response.status == 201){
        this.fetchMediaList()
      } else {
        response.json().then(m => alert(m.error))
      }
    })
  }

  onMediaSelect(media: MediaFile) {
    this.active = media;
    this.workspace.path = ["Media", media.name]
  }
}
