import { Component, ContentChild, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-subheader',
  templateUrl: './subheader.component.html',
  styleUrls: ['./subheader.component.scss']
})
export class SubheaderComponent {
  @ContentChild('main') main: TemplateRef<any> | null = null;
  @ContentChild('right') right: TemplateRef<any> | null = null;
}
