import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { authGuard } from './commons/guards/auth/auth.guard';
import { LoadingComponent } from './pages/loading/loading.component';
import { LayoutComponent } from './commons/components/layout/layout.component';
import { AccountsComponent } from './pages/accounts/accounts.component';
import { DataComponent } from './pages/data/data.component';
import { CommentsComponent } from './pages/comments/comments.component';
import { MonitoringComponent } from './pages/monitoring/monitoring.component';
import { LogsComponent } from './pages/logs/logs.component';
import { SettingsComponent } from './pages/settings/settings.component';
import { HelpComponent } from './pages/help/help.component';
import { AccountSettingsComponent } from './pages/account-settings/account-settings.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  {
    path: '',
    component: LayoutComponent,
    canActivate: [authGuard],
    children: [
      { path: 'dashboard', loadChildren: () => import("./modules/dashboard/dashboard.module").then(m => m.DashboardModule) },
      { path: 'accounts', component: AccountsComponent },
      { path: 'data', component: DataComponent },
      { path: 'media', loadChildren:() => import("./modules/media/media.module").then(m => m.MediaModule) },
      { path: 'comments', component: CommentsComponent },
      { path: 'emails', loadChildren: () => import("./modules/emails/emails.module").then(m => m.EmailsModule) },
      { path: 'analytics', component: MonitoringComponent },
      { path: 'support', component: LogsComponent },
      { path: 'settings', component: SettingsComponent },
      { path: 'help', component: HelpComponent },
      { path: 'account/settings', component: AccountSettingsComponent },
    ]
  },
  { path: '**', component: LoadingComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
