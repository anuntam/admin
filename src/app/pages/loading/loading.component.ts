import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/commons/services/auth.service';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent {
  constructor(private authService: AuthService, private router: Router) {

    if (authService.initializated && !authService.authenticated) {
      this.router.navigateByUrl("/login");
      return;
    }

    authService.ready.subscribe(()=>{
      if (authService.authenticated) {
        this.router.navigateByUrl("/dashboard");
      } else {
        this.router.navigateByUrl("/login");
      }
    });
  }
}
