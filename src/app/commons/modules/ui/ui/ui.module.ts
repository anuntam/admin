import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubheaderComponent } from './subheader/subheader.component';
import { SubheaderButtonComponent } from './subheader-button/subheader-button.component';
import { FormsModule } from '@angular/forms';
import { FilesizePipe } from '../../filesize.pipe';



@NgModule({
  declarations: [
    SubheaderButtonComponent,
    SubheaderComponent,
    FilesizePipe
  ],
  exports: [
    SubheaderButtonComponent,
    SubheaderComponent,
    FilesizePipe
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class UiModule { }
