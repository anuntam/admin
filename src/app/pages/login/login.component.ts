import { Component, inject } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/commons/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  
  email: string = '';
  password: string = '';

  constructor(private authService: AuthService, private router: Router) {
    if (this.authenticated) {
      this.router.navigateByUrl("/dashboard");
    }
  }

  get authenticated() {
    return this.authService.authenticated;
  }

  onLogin() {
    this.authService.auth(this.email, this.password).then(() => {
      this.router.navigateByUrl("/dashboard");
    }).catch((reason) => {
      alert(reason)
    })
  }
}
