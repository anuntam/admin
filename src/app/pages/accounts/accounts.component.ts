import { Component } from '@angular/core';
import { WorkspaceService } from 'src/app/commons/services/workspace.service';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.scss']
})
export class AccountsComponent {
  constructor(workspace: WorkspaceService) {
    workspace.path = ["Accounts"]
  }
}
