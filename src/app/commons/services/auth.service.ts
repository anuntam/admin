import { EventEmitter, Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

const token_name = 'anuntam_admin_token';

interface UserGeneric {
  id: number
  avatar: string
  name: string[]
}

@Injectable({
  providedIn: 'root',
  deps: [CookieService]
})
export class AuthService {

  private __authenticated = false;

  private __authToken = '';

  private __me: UserGeneric | null = null;

  private __initializated = false;

  ready: EventEmitter<boolean> = new EventEmitter(true);

  get authenticated(): boolean {
    return this.__authenticated;
  }

  get me() {
    return this.__me;
  }

  get initializated(): boolean {
    return this.__initializated;
  }

  get accessToken(): string {
    return this.__authToken;
  }

  constructor(private cookies: CookieService) {
    this.verifyAccessToken();
  }

  apply(init?: RequestInit): RequestInit | undefined {

    console.log(init);

    return init
  }

  verifyAccessToken() {

    this.__authToken = this.__authToken || this.cookies.get(token_name)

    const headers = {
      "Authorization": "Bearer " + this.__authToken
    };

    if (this.__authToken) {
      fetch("http://localhost:8085/api/1/me", { headers }).then(async (response) => {
        this.__me = await response.json();
        this.__authenticated = true;
        this.__initializated = true;
        this.ready.emit(true);
      }).catch(() => {
        this.__initializated = true;
        this.ready.emit(true);
      })
    } else {
      this.__initializated = true;
      this.ready.emit(true);
    }
  }

  async auth(email: string, password: string) {

    const response = await fetch("http://localhost:8085/api/1/auth", {
      body: JSON.stringify({ email, password }),
      method: 'POST',
    });

    const data = await response.json()

    if (data.error) {
      return Promise.reject('Authentication failed!');
    }

    if (data.token) {
      this.__authToken = data.token;
      this.cookies.set(token_name, data.token);
      this.__me = data.me;
      this.__authenticated = true;
    }

    return Promise.resolve();
  }

  async fetch(URI: RequestInfo | URL, options?: RequestInit | undefined) {

    const newOptions = {
      ...options,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.__authToken}`,
      },
    }
    const res = await fetch(URI, newOptions);
    return res;
  }

  logout() {
    this.cookies.delete(token_name);
    window.location.href = '/';
  }
}
