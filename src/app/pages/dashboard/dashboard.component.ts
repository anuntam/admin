import { Component, HostListener } from '@angular/core';
import { Subscription } from 'rxjs';
import { WorkspaceService } from 'src/app/commons/services/workspace.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {

  projectChangeSubscription: Subscription | undefined;

  constructor(private workspace: WorkspaceService) {
    workspace.path = ["Dashboard"]
  }

  ngOnInit() {
    // Listen for project changes to update the dasboard
    this.projectChangeSubscription = this.workspace.onProjectChange.subscribe((project) => {
      console.log('Reinitialize workspace for project:', project)
    })
  }

  ngOnDestroy() {
    this.projectChangeSubscription && this.projectChangeSubscription.unsubscribe();
  }
}
