import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainMediaComponent } from './components/main/main.component';
import { RouterModule, Routes } from '@angular/router';
import { UiModule } from 'src/app/commons/modules/ui/ui/ui.module';
import { MediaRendererComponent } from './components/media-renderer/media-renderer.component';

export interface MediaFile {
  uuid:string;
  name: string;
  mime: string;
  size: number;
  crc: string;
  tags: string;
  created_at: Date;
}

const routes: Routes = [
  {
    path: 'manage',
    component: MainMediaComponent,
  },
];

@NgModule({
  declarations: [
    MainMediaComponent,
    MediaRendererComponent
  ],
  imports: [
    CommonModule,
    UiModule,
    RouterModule.forChild(routes)
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MediaModule {

}
