import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './commons/components/header/header.component';
import { LoginComponent } from './pages/login/login.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { FormsModule } from '@angular/forms';
import { LoadingComponent } from './pages/loading/loading.component';
import { LayoutComponent } from './commons/components/layout/layout.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { AccountsComponent } from './pages/accounts/accounts.component';
import {CdkMenuModule} from '@angular/cdk/menu';
import { DataComponent } from './pages/data/data.component';
import { MonitoringComponent } from './pages/monitoring/monitoring.component';
import { LogsComponent } from './pages/logs/logs.component';
import { SettingsComponent } from './pages/settings/settings.component';
import { CommentsComponent } from './pages/comments/comments.component';
import { EmailsComponent } from './pages/emails/emails.component';
import { HelpComponent } from './pages/help/help.component';
import { AccountSettingsComponent } from './pages/account-settings/account-settings.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { SideMenuItemComponent } from './commons/components/side-menu-item/side-menu-item.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginComponent,
    DashboardComponent,
    LoadingComponent,
    LayoutComponent,
    AccountsComponent,
    DataComponent,
    MonitoringComponent,
    LogsComponent,
    SettingsComponent,
    CommentsComponent,
    EmailsComponent,
    HelpComponent,
    AccountSettingsComponent,
    SideMenuItemComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    NoopAnimationsModule,
    CdkMenuModule,
    MatProgressSpinnerModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
