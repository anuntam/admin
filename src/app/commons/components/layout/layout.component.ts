import { Component, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem } from '../side-menu-item/side-menu-item.component';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent {

  menu: MenuItem[] = [
    { path: "/emails", text: "Props", icon: "toggle_on"},
    { path: "/emails", text: "Ads", icon: "database", children:[
      { path: "/emails", text: "Approvals" },
      { path: "/emails", text: "Flagged" },
    ] },
  ]

  constructor(router: Router) {
    if (router.url === '/') {
      router.navigateByUrl('/dashboard');
    }
  }
}
