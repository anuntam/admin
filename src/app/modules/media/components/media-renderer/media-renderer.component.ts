import { Component, EventEmitter, HostBinding, HostListener, Input, Output } from '@angular/core';

export interface MediaFile {
  uuid:string;
  name: string;
  mime: string;
  size: number;
  crc: string;
  tags: string;
  description: string;
  created_at: Date;
}

@Component({
  selector: 'app-media-renderer',
  templateUrl: './media-renderer.component.html',
  styleUrls: ['./media-renderer.component.scss']
})
export class MediaRendererComponent {

  @Input() float: boolean = false;

  @HostBinding("style.float")
  get floating() {
    return this.float ? 'left' : 'none';
  }

  @HostBinding("style.width.px")
  @Input() width: number = 250;

  @HostBinding("style.height.px")
  @Input() height: number = 170;

  @Input() media?: MediaFile;

  @Output() click: EventEmitter<MediaFile> = new EventEmitter();

  @HostListener('click')
  onClick() {
    if (this.media) {
      this.click.next(this.media);
    }
  }

  get thumbnail(): string {
    const m = (uuid: string) => "http://localhost:8000/media/" + uuid;
    switch(this.media?.mime) {
      case "image/png":
      case "image/jpeg":
      case "image/jpg":
        return m(this.media?.uuid);
    }

    return "";
  }

  get icon(): string {
    if (this.media) {
      switch(this.media.mime) {
        case "image/png":
        case "image/jpeg":
        case "image/jpg":
          return "image";
      }
    }
    return "note";
  }
}
