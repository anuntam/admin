import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

export const authGuard: CanActivateFn = (route, state) => {

  const auth = inject(AuthService);
  const router = inject(Router);

  if (auth.initializated && !auth.authenticated) {
    router.navigateByUrl('/login');
    return false;
  }

  if (!auth.initializated) {
    return new Promise((resolve, reject) => {
      auth.ready.subscribe(() => {
        
        if (!auth.authenticated) {
          router.navigateByUrl('/login');
          resolve(false);
          return
        }

        resolve(true);
      });
    })
  }

  return true;

};
