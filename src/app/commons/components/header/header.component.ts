import { Component } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { WorkspaceService } from '../../services/workspace.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  constructor(
    private auth: AuthService,
    public workspace: WorkspaceService,
    private router: Router
  ) {}

  get avatarURL() {
    if (this.auth.me) {
      return 'http://localhost:8000/media/' + this.auth.me.avatar;
    }
    return '';
  }

  onChangeProject(project: any) {
    this.workspace.project = project;
  }

  async createProject(name: string) {
    fetch('http://localhost:8000/app/projects?project=' + name, {
      method: 'PUT',
      headers:{'Authorization': 'Bearer ' + this.auth.accessToken},
    }).then(()=>{
      this.workspace.fetchProjects().then(() => {
        this.workspace.project = this.workspace.projects[this.workspace.projects.length - 1];
      });
    })
  }

  logout() {
    this.auth.logout();
  }
}
