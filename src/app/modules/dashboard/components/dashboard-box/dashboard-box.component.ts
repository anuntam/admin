import { Component, HostBinding, Input } from '@angular/core';

@Component({
  selector: 'app-dashboard-box',
  templateUrl: './dashboard-box.component.html',
  styleUrls: ['./dashboard-box.component.scss']
})
export class DashboardBoxComponent {

  @HostBinding("style.gridArea")
  @Input() area?: string;

  @Input() headline?: string;
}
